import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView, SafeAreaView, TouchableOpacity, Image,FlatList } from 'react-native'
import SearcherBar from '../atoms/searcherBar/SearcherBar'
import { searchMovies, searchMoviesWithPage } from '../../service/Service'

const SearchPage = (props) => {
    const [movies, setMovies] = useState([])
    const [inputMovie, setInputMovie] = useState()
    const [numberPage, setNumberPage] = useState(1)
        
    const updateInputMovie =(inputSearch) =>{
        setNumberPage(1)
        setMovies([])
       
        if(inputSearch.length > 3){
            setInputMovie(inputSearch)
            searchMovies(inputSearch, numberPage).then(res=>{
                setMovies(res.data.Search)
                
            })
        }
        else{
            setMovies([])
        }
    }

    const handeLoadMore = () =>{
        
        searchMovies(inputMovie, (parseInt(numberPage)+1)).then(res=>{
            setNumberPage((parseInt(numberPage)+1))
            setMovies([...movies, ...res.data.Search])
           // console.log(numberPage)
        })
    }

      function Item({ movie }) {
        return (
         
                <TouchableOpacity onPress={()=>{
                    props.navigation.navigate({routeName: 'DetailPage', 
                    params: {
                        movieId: movie.imdbID
                    }})
                }}>
                    {movie.Poster == "N/A" ? <Image style={{width: 100, height: 150, marginHorizontal: 15, marginBottom: 30}}  source={{uri: 'https://lh3.googleusercontent.com/proxy/fptXWYKcjeRJIJN9A5XFQ-q48scSjd6erQAXUIqfOrY31uYUCQGC2LlC7dl6gAkKej3jhSz63MfoPwXZCVC2b9bCJ3uKzpnhBPTnaRw29thZy2XhzRRQP0U' }} /> : <Image style={{width: 100, height: 150, marginHorizontal: 15, marginBottom: 30}}  source={{uri: movie.Poster }} />}
                    
                </TouchableOpacity>
           
        );
      }
    
    return (
        <View>
            <SearcherBar updateInputMovie={updateInputMovie}/>
            <View style={styles.container} >
                { movies == undefined ? <Text style={styles.text}>No results were found</Text> : <Text style={styles.text}></Text> }
            </View>
            <SafeAreaView>
              
                <FlatList
                    data={movies}
                    renderItem={({ item }) => <Item movie={item} />}
                    keyExtractor={item => {console.log('itemid', item.imdbID); return item.imdbID}}
                    numColumns={3}
                    onEndReached={handeLoadMore}
                />
              
            </SafeAreaView>
        </View>  
    )
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
    },
    text:{
        fontSize: 20,
        marginTop: 15
    },
    
  })

export default SearchPage