import React from 'react';
import { View} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { createAppContainer } from 'react-navigation'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'

import HomePage from '../homepage/HomePage'
import SearchPage from '../searchpage/SearchPage'
import DetailPage from '../detailPage/DetailPage'

import { createStackNavigator } from 'react-navigation-stack'

const SearchStack = createStackNavigator({ 
    SearchPage: SearchPage,
    DetailPage: DetailPage
 });

 SearchStack.navigationOptions = {
  tabBarLabel: 'Search',
  tabBarIcon: () => (
    <View>
     <FontAwesome name="search" size={30} color='#fff' />
    </View>
  ),
};

const TabNavigator = createMaterialBottomTabNavigator({
  
  Home: {
    screen: HomePage,
    navigationOptions:{
      tabBarIcon: () => (
        <View>
         <FontAwesome name="home" size={30} color='#fff'  />
        </View>
      ),
    }
  },
  SearchStack


}, 
{
  initialRouteName: 'Home',
  activeColor: '#fff',
  inactiveColor: '#ff3333',
  barStyle: { backgroundColor: '#ff3333'},
  
})

export default createAppContainer ( TabNavigator )
