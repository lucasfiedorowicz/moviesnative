import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, SafeAreaView, ScrollView, Image } from 'react-native'
import { getOneMovie } from '../../service/Service'

const DetailPage = (props) => {
    const [movie, setMovie] = useState({})
    const movieId = props.navigation.getParam('movieId')
   
    useEffect(()=>{
       getOneMovie(movieId).then( resMovie => {
            setMovie(resMovie.data)
          
        });
    },[])

    /*getOneMovie('inputSearch').then(res=>{
        console.log(res.data)
    })*/
   
   
    return (
        <SafeAreaView>
            <ScrollView >
                <View style={styles.container}>
                    <Image style={{width: 200, height: 250, marginTop: 15, marginBottom: 15}}  source={{uri: movie.Poster }} />
                    <Text style={styles.title}>{movie.Title}</Text>
                </View>
                <View style={styles.containerDetails}>
                    <Text style={styles.basicInfo}>{movie.Year}</Text>
                    <Text style={styles.basicInfo}>{movie.Rated}</Text>
                    <Text style={styles.basicInfo}>{movie.Runtime}</Text>
                </View>
                <Text style={styles.plot}>{movie.Plot}</Text>
                <Text style={{marginLeft: 10, marginTop: 10}}>
                   <Text style={{fontWeight: 'bold', fontSize: 15}}>Casting:</Text> 
                   <Text> {movie.Actors}</Text>
                </Text>
                <Text style={{marginLeft: 10, marginTop: 10}}>
                   <Text style={{fontWeight: 'bold', fontSize: 15}}>Director:</Text> 
                   <Text> {movie.Director}</Text>
                </Text>
                <Text style={{fontWeight: 'bold', fontSize: 15, marginLeft: 10, marginTop: 10, marginBottom: 20}}>{movie.Genre}</Text> 
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerDetails: {
        flex: 1,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignSelf: 'flex-start'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        marginBottom: 15
    },
    basicInfo: {
        //backgroundColor: '#FFA07A',
        marginLeft: 10,
        fontSize: 17,
        fontWeight: 'bold'
    },
    plot: {
        marginTop: 5,
        fontSize: 17,
        paddingLeft: 10,
    },
    extrInfo: {

    }
})

export default DetailPage