import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native'



const HomePage = (props) => {
  


  return (
      <View style={styles.container}>
        <Image
          style={{width: 100, height: 100}}
          source={{uri: 'https://icons.iconarchive.com/icons/graphicloads/medical-health/256/search-disease-icon.png'}}
        />
        <Text style={styles.text}> Movie Searcher </Text>
       {/* <Button title='Search...' onPress={ ()=>{
          props.navigation.navigate({routeName: 'DetailPage'})
        }}/>*/}
      </View>  
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ff4d4d',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        marginTop: 10
    },
  })

export default HomePage