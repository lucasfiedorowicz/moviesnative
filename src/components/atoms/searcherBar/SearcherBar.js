import React from 'react'
import { SearchBar } from 'react-native-elements';

const SearcherBar = ({updateInputMovie}) =>  {
  
  const { search } = '';

  const updateSearch = search => {
    
    updateInputMovie(search)

  };
  
   

    return (
      <SearchBar
        onChangeText={updateSearch}
        value={search}

        inputStyle={{backgroundColor: 'white', color:'#000'}}
        containerStyle={{backgroundColor: '#ff4d4d', borderWidth: 1, borderRadius: 5}}
        placeholderTextColor={'#ff4d4d'}
        inputContainerStyle={{backgroundColor: '#fff'}}
        cancelButtonTitle='Cancel'
        searchIcon={{color: '#000'}}
        placeholder="Search movie..."
      
      />
    );
  
}
SearcherBar.defaultProps={updateInputMovie:()=>{

}}
export default SearcherBar



