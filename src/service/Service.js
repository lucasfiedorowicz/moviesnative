import axios from 'axios'

const apiKey = '?apikey=51e5e8bb'

const moviesService = axios.create({
    baseURL: 'http://www.omdbapi.com/',
    //timeout: 1000,
    //headers: {'X-Custom-Header': 'foobar'}
  });

const searchMovies = (keyword, numberPage) => {
    return moviesService.get(`${apiKey}&s=${keyword}&page=${numberPage}`)
}

const searchMoviesWithPage = (keyword, numberPage) => {
    return moviesService.get(`${apiKey}&s=${keyword}&page=${numberPage}`)
}

const getOneMovie = (movieId) => {
    return moviesService.get(`${apiKey}&i=${movieId}`)
}

export {
    searchMovies,
    getOneMovie,
    searchMoviesWithPage
} 